import mongoose from "mongoose";
import Profil from "../models/profil.js"
import auth from "../middleware/auth.js";

export const getProfils = ([auth], async (req,res) => {
    const userId = req.userId;
    try {
        console.log('idUser : '+ userId)
        const profils = await Profil.find(({'_id':{'$nin':  [userId]}}));
        res.status(200).json(profils)
    } catch (error) {
        res.status(404).json({ message : error.message })
    }

})

export const updateProfil = async (req, res) => {
    const userId = req.userId;
    const profil = req.body;

    if (!mongoose.Types.ObjectId.isValid(userId))
    return res.status(404).send('no profil with that ID')

    const updatedProfil = await Profil.findByIdAndUpdate(userId, {...profil, userId}, {new : true});

    res.json(updateProfil);
};
;