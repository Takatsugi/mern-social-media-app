import mongoose from "mongoose";
import Message from "../models/message.js"

export const getMessages = async (req,res) => {
    try {
        const messages = await Message.find();
        res.status(200).json(messages)
    } catch (error) {
        res.status(404).json({ message : error.message })
    }

};

export const createMessage = async (req, res) => {
    const message = req.body;
    const newMessage = new Message({...message, sender: req.userId, createdAt: new Date().toISOString()})
    try {
        await newMessage.save()
        res.status(201).json(newMessage)
    } catch (error) {
        res.status(409).json({ message : error.message })
        
    }
};


export const deleteMessage = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).send('no messages with that ID')

    await Message.findByIdAndRemove(id);

    res.json({message: 'message deleted'});
};

export const likeMessage = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
    return res.status(404).send('no message with that ID')

    const message = await Message.findById(id)
    const updatedMessage = await Message.findByIdAndUpdate(id, {likeCount: message.likeCount= true}, {new: true})


    res.json(updatedMessage);
};