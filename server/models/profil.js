import mongoose from "mongoose";
const profilSchema = mongoose.Schema({
    id: {type: String},
    name: {type: String, required:true},
    nickname: String,
    profilImage: String,
    coverImage: String,
    uploadedImages: String,
    intro: String,
    hobbies: String,
    townCity: String,
    phoneNumber1: {type: String, required:true},
    phoneNumber2: String,
    gender: Boolean,
    birthday: {type: Date, required:true},
    languages: [String],
    interestedIn: Boolean,
    work: String,
    education: String,
    relationship: Boolean,
    friends: [{ type: mongoose.Types.ObjectId, ref: "Profil" }],
    posts: [{ type: mongoose.Types.ObjectId, ref: "PostMessage" }],
    createdAt: {
        type: Date,
        default: new Date(),
      }
});

export default mongoose.model("Profil", profilSchema);
