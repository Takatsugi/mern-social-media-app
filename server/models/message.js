import mongoose from "mongoose";

const messageSchema = mongoose.Schema({
  message: String,
  sender: String,
  sentTo: String,
  selectedFile: String,
  likeCount: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
});

const message = mongoose.model("Message", messageSchema);
export default message;
