const users = [];

export const addUser = ({user, socketId}) => {
const existingUser = users.find((u) => u?.user?.result?._id === user?.user?.result?._id);
if (existingUser)
{
    console.log('user already exists')
    return {error : 'user already exists'};
}
const u = {socketId,user:user.user};
console.log(u?.user?.result?._id)
users.push(u);
return {u};
};

export const removeUser = (socketId) => {
    const index = users.findIndex((user) => user.socketId === socketId);
    if (index !== -1){
        return users.splice(index, 1)[0];
    }
};
export const getUser = (socketId) => users.find((user) => user.socketId === socketId);

export const removeUsersInRoom = () => {

};
