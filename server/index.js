import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv'
import {Socket, Server} from 'socket.io'
import * as http from 'http';
import userRoutes from './routes/users.js';
import postRoutes from './routes/posts.js';
import messageRoute from './routes/messages.js';
import profilRoute from './routes/profil.js';
//import router from './router.js';

import {addUser, removeUser, getUser} from './users.js'
const app = express();
const server = http.createServer(app);
const io = new Server(server, {
  cors: {
  origin: '*',
  methods: 'GET,PUT,POST,DELETE,OPTIONS'.split(','),
  credentials: true
}
});
dotenv.config();
  
app.use(bodyParser.json({limit: "30mb", extended: true}));
app.use(bodyParser.urlencoded({limit: "30mb", extended: true}));
app.use(cors());
//app.use(router);

io.on('connection', (socket) => {
  console.log('new connection!!!')
  socket.on('user', (user, callback) => {
    const {error, socket_user} = addUser({user, socketId: socket.id});
    //console.log(user?.user?.result?.name)
    //if (error) return callback(error);
  });

  socket.on('sendMessage', (message, callback) => {
    const user = getUser(socket.id)
  });

  socket.on('disconnect', () => {
    const user = removeUser(socket.id);
    
    console.log('user left!!!')
  })
});

//const CONNECTION_URL = 'mongodb+srv://hatem:1vEhjlMDt8uE0vGU@cluster1.ah2bd.mongodb.net/?retryWrites=true&w=majority';
const PORT = process.env.PORT || 5000;

const SOCKET_PORT = process.env.SOCKET_PORT || 5001;

server.listen(SOCKET_PORT, () => console.log(`Socket server has started on port ${SOCKET_PORT}`));

app.use('/user', userRoutes);
app.use('/posts', postRoutes);
app.use('/messages',messageRoute);
app.use('/profil',profilRoute);


mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => app.listen(PORT, () => console.log(`Server Running on Port: http://localhost:${PORT}`)))
  .catch((error) => console.log(`${error} did not connect`));



