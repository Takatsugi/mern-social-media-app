import express from "express";
import { getProfils, updateProfil } from '../controllers/profil.js';

const router = express.Router();

router.get('/', getProfils);
router.patch('/', updateProfil);

export default router;