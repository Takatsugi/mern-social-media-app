import express from "express";
import { getMessages, createMessage, deleteMessage, likeMessage } from '../controllers/messages.js';

const router = express.Router();


router.get('/', getMessages);
router.post('/', createMessage);
router.delete('/:id', deleteMessage);
router.patch('/:id/likePost', likeMessage);




export default router;