import React, {useEffect} from "react";
import Auth from "./components/Auth/Auth";
import Home from './components/Home/Home';
//import Messenger from './components/Messenger/Messenger'
import Messenger from "./components/MessengerComponents/Messenger/Messenger";
import { BrowserRouter, Routes , Route } from 'react-router-dom';
import useStyles from './styles';
import { Container, AppBar, Typography, Grow, Grid } from "@material-ui/core";
import socialmeee from './images/socialmeee.jpg'
import Posts from "./components/Posts/Posts";
import { getPosts } from "./actions/posts";
import Form from "./components/Form/Form";
import { useDispatch } from "react-redux";
import Navbar from "./components/Navbar/Navbar";
import io from 'socket.io-client';
import Friends from "./components/Friends/Friends";

let socket;

const App = () => {
  const user = JSON.parse(localStorage.getItem('profile'));
  const ENDPOINT = 'localhost:5001';
  const classes = useStyles();

  

  useEffect(() => {
    socket = io(ENDPOINT);
    socket.emit('user', {user}, ({error}) => {
      console.log('wa')
    });
    console.log(user?.result?.name)
    console.log(socket)

    return () => {
      socket.emit('disconnect');
      socket.off();
    }
  }, [ENDPOINT]);
  return (
    <BrowserRouter>
    <Container className={classes.container} maxWidth="xl" >
  <Navbar/>      
<div style={{marginTop:'56px'}}> 
        <Routes >
        <Route path="/" exact element={<Home/>} />
        <Route path="/auth" exact element={<Auth/>} />
        <Route path="/messenger" exact element={<Messenger/>} />
        <Route path="/friends" exact element={<Friends/>} />
        {/* <Route path="/MessengerLayout" exact element={<MessengerLayout/>} /> */}
        
        </Routes>

        </div>
    </Container>
  </BrowserRouter>
  )}

export default App;