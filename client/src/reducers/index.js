import { combineReducers } from 'redux'
import auth from './auth'
import posts from './posts';
import messages from './messages'
import profil from './profil';

export const reducers = combineReducers({auth,posts,messages,profil});