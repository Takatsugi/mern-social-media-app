import { FETCH_ALL,CREATE,DELETE,LIKEMESSAGE } from '../constants/actionTypes';

// eslint-disable-next-line import/no-anonymous-default-export
export default (messages = [],action) => {
    switch (action.type) {
        case FETCH_ALL:
        return action.payload;    
        case CREATE:
        return [...messages, action.payload];
        case LIKEMESSAGE:
        return messages.map((message) => (message._id === action.payload._id ? action.payload : message));
        case DELETE:
        return messages.filter((message) => message._id !== action.payload);
        default:
            return messages;
    }
}