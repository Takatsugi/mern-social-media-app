import { FETCH_ALL, UPDATE} from '../constants/actionTypes';

// eslint-disable-next-line import/no-anonymous-default-export
export default (profils = [],action) => {
    switch (action.type) {
        case FETCH_ALL:
        return action.payload;    
        case UPDATE:
        return profils.map((profil) => (profil._id === action.payload._id ? action.payload : profil));
        default:
            return profils;
    }
}