import React, { useState } from 'react';
import { Socket } from 'socket.io-client';
import InfoBar from '../InfoBar/InfoBar';
import './Messenger.css'
import Input from '../Input/Input';
import Messages from '../Messages/Messages'

const Messenger = () => {
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);

  const sendMessage = (event) => {
    event.preventDefault();
    if (message){
      Socket.emit('sendMessage', message, () => setMessage(''))
    }
  }
  console.log(message, messages)
  return (
    <div className='outerContainer'>
      <div className='container'>
        <InfoBar/>
        <Messages messages={messages}/>
        <Input message={message} setMessage={setMessage} sendMessag={sendMessage}/>
      </div>
    </div>
  );
};

export default Messenger;