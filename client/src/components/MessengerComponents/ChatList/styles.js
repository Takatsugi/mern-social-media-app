import { makeStyles } from '@material-ui/core/styles';
import zIndex from '@material-ui/core/styles/zIndex';

export default makeStyles((theme) => ({
  spacing: 8,
  paper: {
    height: 'calc(100vh )',
    padding: theme.spacing(1),
    borderRadius: '0px',
    position: 'fixed',
    width:'32%'
   // backgroundColor: 'transparent'
  },
  container: {
    width: '100%',
   // backgroundColor: 'transparent',
  },
  typography: {
    lineHeight: '0.6',
    fontWeight: '600'
  },
  textField: {
    width: '97%',
    margin: '10px 0',
    height: '36px',
    backgroundColor: '#f4f4f8 ' ,
  borderRadius: '10px',
  border: 'none',
  fontSize: '14px',
  paddingTop: '5px',
  //border: '0px'
  },
  conversation_list_item: {
    display: 'flex',
    alignItems: 'center',
    padding: '10px',
    '&:hover': {
        background: '#eeeef1',
        cursor: 'pointer',
      },
  },
  conversation_name: {
    display: 'flex',
    alignItems: 'center',
    '&:hover': {
        background: '#eeeef1',
        cursor: 'pointer',
      },
  },
  
  conversation_photo: {
    width: '50px',
    height: '50px',
    borderRadius: '50%',
    objectFit: 'cover',
    marginRight: '10px',
  },
  
  conversation_title: {
    fontSize: '14px',
    fontWeight: 'bold',
    textTransform: 'capitalize',
    margin: '0',
  },
  
  conversation_snippet: {
    fontSize: '14px',
    color: '#888',
    margin: '0',
  },
  div: {
    zIndex: 5,
    position: 'flex',
    overflow: 'auto',
    height: 'calc(100vh - 211px)',
    maxHeight: 'calc(100vh - 211px)' 
  },
  
  
  leftIconGrid: {
    marginTop: '10px',
    marginBottom: '5px',
  },
  
}));