import React from 'react';
import {
    Paper,
    Grid,
    Typography,
    TextField,
  } from "@material-ui/core";
  import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import VideoCallIcon from "@material-ui/icons/VideoCall";
import EditIcon from "@material-ui/icons/Edit";
import useStyles from "./styles";
import avatar from "../../../images/avatar.png";

const ChatList = () => {
    const classes = useStyles();
    return (
        <Paper className={classes.paper}>
          <Grid container className={classes.leftIconGrid}>
            <Grid item xs={9}>
              <Typography className={classes.typography} variant="h4">
                Chats
              </Typography>
            </Grid>
            <Grid item xs={1}>
              <MoreHorizIcon />
            </Grid>
            <Grid item xs={1}>
              <VideoCallIcon />
            </Grid>
            <Grid item xs={1}>
              <EditIcon />
            </Grid>
          </Grid>
          <TextField
            placeholder="Search in messenger"
            className={classes.textField}
            InputProps={{
              disableUnderline: true,
              startAdornment: (
                <InputAdornment className="standard" position="start">
                  <SearchIcon />
                </InputAdornment>
              ),
            }}
          />
          <div className={classes.div}>
            <div className={classes.conversation_list_item}>
              <img
                className={classes.conversation_photo}
                src={avatar}
                alt="conversation"
              />
              <div className={classes.conversation_info}>
                <h1 className={classes.conversation_title}>hatem</h1>
                <p className={classes.conversation_snippet}>hello there</p>
              </div>
            </div>
          </div>
        </Paper>
    );
};

export default ChatList;