import { makeStyles } from "@material-ui/core/styles";
import zIndex from "@material-ui/core/styles/zIndex";

export default makeStyles((theme) => ({
  spacing: 8,
  

  div: {
    zIndex: 5,
    position: "flex",
    overflow: "auto",
    height: "calc(100vh - 211px)",
    maxHeight: "calc(100vh - 211px)",
  },
}));
