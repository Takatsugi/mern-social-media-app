import React, { useState } from "react";
import { Grid, Container } from "@material-ui/core";

import ChatList from "../ChatList/ChatList";
import MessageListContainer from "../MessageListContainer/MessageListContainer";

import useStyles from "./styles";
const MessengerLayout = () => {
  const classes = useStyles();

  return (
      <Grid container className={classes.container}>
        <Grid item xs={4}>
          <ChatList />
        </Grid>
        <Grid item xs={8}>
            <MessageListContainer />
        </Grid>
      </Grid>
  );
};

export default MessengerLayout;
