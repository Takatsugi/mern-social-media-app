import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  topBar: {
    position: 'fixed',
    width: '67%',
    backgroundColor: 'white',
    top: '55px',
    height: '60px',
  },
  conversation_list_item: {
    display: "flex",
    alignItems: "center",
    padding: "10px",
    "&:hover": {
      background: "#eeeef1",
      cursor: "pointer",
    },
  },
  conversation_name: {
    display: "flex",
    alignItems: "center",
    "&:hover": {
      background: "#eeeef1",
      cursor: "pointer",
    },
  },

  conversation_photo: {
    width: "50px",
    height: "50px",
    borderRadius: "50%",
    objectFit: "cover",
    marginRight: "10px",
    marginTop: "10px",
    marginLeft: '10px'
  },

  conversation_title: {
    fontSize: "14px",
    fontWeight: "bold",
    textTransform: "capitalize",
    margin: "0",
  },

  conversation_snippet: {
    fontSize: "14px",
    color: "#888",
    margin: "0",
  },
  rightIconDivWidth: {
    width: "25%",
    marginTop: "10px",
  },
}));
