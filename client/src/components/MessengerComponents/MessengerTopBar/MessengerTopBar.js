import React from 'react';
import useStyles from "./styles";
import avatar from "../../../images/avatar.png";
import {
    Grid
  } from "@material-ui/core";
  import CallIcon from "@material-ui/icons/Call";
import VideocamIcon from "@material-ui/icons/Videocam";
import InfoIcon from "@material-ui/icons/Info";
const MessengerTopBar = () => {
    const classes = useStyles();
    return (
        <>
        <Grid container className={classes.topBar}>
            <Grid item xs={9}>
        <div className={classes.conversation_name}>
                <img
                  className={classes.conversation_photo}
                  src={avatar}
                  alt="conversation"
                />
                <div className={classes.conversation_info}>
                  <h1 className={classes.conversation_title}>Hatem Abrouz</h1>
                  <p className={classes.conversation_snippet}>
                    Active 24 min ago
                  </p>
                </div>
                </div>
                </Grid>

                <div className={classes.rightIconDivWidth}>
              <Grid container>
                <Grid item xs={3}></Grid>
                <Grid item xs={3}>
                  <CallIcon color="primary"/>
                </Grid>
                <Grid item xs={3}>
                  <VideocamIcon color="primary"/>
                </Grid>
                <Grid className={classes.infoIcon} item xs={3}>
                  <InfoIcon color="primary"/>
                </Grid>
              </Grid>
            </div>
            </Grid>

            </>
              
    );
};

export default MessengerTopBar;