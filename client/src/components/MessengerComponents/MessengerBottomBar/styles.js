import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
bottomInputContainer: {
    position: 'fixed',
    bottom: '0px',
    backgroundColor: 'white',
    maxWidth: '65%',
  },
  bottomTextField:{
    paddingTop: '5px',
    paddingLeft: '5px',
    margin: '10px 0',
    height: '36px',
    backgroundColor: '#f4f4f8 ' ,
  borderRadius: '10px',
  border: 'none',
  fontSize: '14px',
  width: '100%',
  },
  bottomIconDivWidth: {
    width: '10%',
    marginTop: '17px',
    display: 'flex',
    justifyContent: 'center'
  },
  bottomLikeIcon: {

  },
  bottomInput: {
    padding: '10px',
  display: 'flex',
  alignItems: 'center',
  background: 'white',
  borderTop: '1px solid #eeeef1',
  width: 'calc(100% - 20px)',
 
  },
}));