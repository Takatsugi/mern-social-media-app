import React from 'react';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import GifIcon from '@material-ui/icons/Gif';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import InputAdornment from "@material-ui/core/InputAdornment";
import {
    Grid,
    TextField,
  } from "@material-ui/core";
  import useStyles from "./styles";

const MessengerBottomBar = () => {
    const classes = useStyles();
    return (
        <div>
            <Grid container className={classes.bottomInputContainer}>
            <Grid item xs={2} className={classes.bottomIconDivWidth}>
           <Grid item xs={4}>
              <AddCircleIcon color="primary" />
            </Grid>
            <Grid item xs={4}>
              <PhotoLibraryIcon color="primary" />
            </Grid>
            <Grid item xs={4}>
              <GifIcon color="primary" />
            </Grid>
            </Grid>
            <Grid item xs={9}>
            <TextField
            placeholder="Aa"
            className={classes.bottomTextField}
            InputProps={{
              disableUnderline: true,
              endAdornment: (
                <InputAdornment className="standard" position="start">
                  <InsertEmoticonIcon color="primary"/>
                </InputAdornment>
              ),
            }}
          />
        </Grid >
        <Grid item xs={1} className={classes.bottomIconDivWidth}>
              <ThumbUpIcon color="primary" />
            </Grid>
        </Grid>
            
          </div>
    );
};

export default MessengerBottomBar;