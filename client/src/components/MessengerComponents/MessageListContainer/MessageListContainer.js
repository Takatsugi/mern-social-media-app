import React from "react";

import Message from "./Message/Message";
import MessengerTopBar from "../MessengerTopBar/MessengerTopBar";
import { Paper } from "@material-ui/core";
import useStyles from "./styles";

const MessageListContainer = () => {
    const classes = useStyles();
  return (
    <Paper className={classes.paper}>
    <MessengerTopBar/>
      <Message />
      </Paper>
  );
};

export default MessageListContainer;
