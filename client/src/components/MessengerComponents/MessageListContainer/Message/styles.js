
import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
bubble_container_mine: {
  fontSize: '14px',
  display: 'flex',
  marginBottom: '10px',
  justifyContent: 'flex-end'
},
bubble_container_not_mine: {
  fontSize: '14px',
  display: 'flex',
  marginBottom: '10px',
  justifyContent: 'flex-start',
  
},
bubble_mine: {
  background: '#007aff',
color: 'white',
borderTopLeftRadius: '20px',
borderBottomLeftRadius: '20px',
borderTopRightRadius: '20px',
borderBottomRightRadius: '20px',
padding: '10px 15px',
maxWidth: '80%',

},
bubble_not_mine: {
margin: '1px 0px',
background: '#f4f4f8',
padding: '10px 15px',
borderRadius: '20px',
maxWidth: '80%',
borderTopLeftRadius: '20px',
borderBottomLeftRadius: '20px',
borderTopRightRadius: '20px',
borderBottomRightRadius: '20px',

},
message_list_container: {
  marginTop: '51px',
  padding: '10px',
  paddingBottom: '70px',
  backgroundColor: 'white',
  height: '67%',
  overflow: 'auto',
},
message: {
  display: 'flex',
  flexDirection: 'column',
},
}));
/* .textContainer {
    display: flex;
    flex-direction: column;
    margin-left: 100px;
    color: white;
    height: 60%;
    justify-content: space-between;
  }
  
  .activeContainer {
    display: flex;
    align-items: center;
    margin-bottom: 50%;
  }
  
  .activeItem {
    display: flex;
    align-items: center;
  }
  
  .activeContainer img {
    padding-left: 10px;
  }
  
  .textContainer h1 {
    margin-bottom: 0px;
  }
  
  @media (min-width: 320px) and (max-width: 1200px) {
    .textContainer {
      display: none;
    }
  } */