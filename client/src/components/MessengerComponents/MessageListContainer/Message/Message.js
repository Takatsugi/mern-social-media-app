import React from 'react';
import useStyles from "./styles";
import MessengerBottomBar from '../../MessengerBottomBar/MessengerBottomBar';

const Message = () => {
    const classes = useStyles();
    return (
        <>
        <div className={classes.message_list_container}>
        <div className={classes.message}>
        <div className={classes.bubble_container_mine}>
                <div className={classes.bubble_mine}>hello there </div>
              </div>
        <div className={classes.bubble_container_mine}>
                <div className={classes.bubble_mine}>
                  Hello world! This is a long message that will hopefully get
                  wrapped by our message bubble component! We will see how well
                  it works.
                </div>
              </div>
              <div className={classes.bubble_container_not_mine}>
                <div className={classes.bubble_not_mine}>
                  Hello world! This is a long message that will hopefully get
                  wrapped by our message bubble component! We will see how well
                  it works.
                </div>
              </div><div className={classes.bubble_container_not_mine}>
                <div className={classes.bubble_not_mine}>
                  Hello world! This is a long message that will hopefully get
                  wrapped by our message bubble component! We will see how well
                  it works.
                </div>
              </div><div className={classes.bubble_container_not_mine}>
                <div className={classes.bubble_not_mine}>
                  Hello world! This is a long message that will hopefully get
                  wrapped by our message bubble component! We will see how well
                  it works.
                </div>
              </div><div className={classes.bubble_container_not_mine}>
                <div className={classes.bubble_not_mine}>
                  Hello world! This is a long message that will hopefully get
                  wrapped by our message bubble component! We will see how well
                  it works.
                </div>
              </div><div className={classes.bubble_container_not_mine}>
                <div className={classes.bubble_not_mine}>
                  Hello world! This is a long message that will hopefully get
                  wrapped by our message bubble component! We will see how well
                  it works.
                </div>
              </div><div className={classes.bubble_container_not_mine}>
                <div className={classes.bubble_not_mine}>
                  Hello world! This is a long message that will hopefully get
                  wrapped by our message bubble component! We will see how well
                  it works.
                </div>
              </div><div className={classes.bubble_container_not_mine}>
                <div className={classes.bubble_not_mine}>
                  Hello world! This is a long message that will hopefully get
                  wrapped by our message bubble component! We will see how well
                  it works.
                </div>
              </div>
              <MessengerBottomBar/>

              </div>

              </div>
              </>
    );
};

export default Message;

/* import React from 'react';
import './Message.css'
import ReactEmoji from 'react-emoji'

const Message = ({message: {text}}) => {
    const user = JSON.parse(localStorage.getItem('profile'));
    let isSentByCurrentUser = false
    return (
        <div className='messageContainer justifyEnd'>
            <p className='sentText pr-10'>{user?.result?.name}</p>
            <div className='messageBox backgroundBlue'>
                <p className='messageText colorWhite'>{ReactEmoji.emojify(text)}</p>
            </div>
        </div>
    );
};

export default Message; */