import React from 'react';
import FriendsLeftBar from './FriendsLeftBar/FriendsLeftBar';
import MayKnow from './MayKnow/MayKnow';
import { Grid } from "@material-ui/core";

const Friends = () => {
    return (
    <Grid container>
        <Grid item xs={3}>
            <FriendsLeftBar/>
        </Grid>
        <Grid item xs={9}>
            <MayKnow/>
        </Grid>
    </Grid>
    );
};

export default Friends;