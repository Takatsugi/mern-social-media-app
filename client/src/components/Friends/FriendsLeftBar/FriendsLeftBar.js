import React from "react";
import { Paper, Grid, Typography, TextField } from "@material-ui/core";
import SettingsIcon from "@material-ui/icons/Settings";
import PeopleIcon from "@material-ui/icons/People";
import CallReceivedIcon from "@material-ui/icons/CallReceived";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import FormatAlignJustifyIcon from "@material-ui/icons/FormatAlignJustify";
import CakeIcon from "@material-ui/icons/Cake";
import useStyles from "./styles";
import Avatar from "@material-ui/core/Avatar";
import avatar from "../../../images/avatar.png";

const FriendsLeftBar = () => {
  const classes = useStyles();
  return (
        <Paper className={classes.paper}>
          <Grid container className={classes.leftIconGrid}>
            <Grid item xs={10} lg={11}>
              <Typography className={classes.typography} variant="h4">
                Friends
              </Typography>
            </Grid>
            <Grid item>
              <SettingsIcon />
            </Grid>
          </Grid>

          <div className={classes.list_item}>
            <Avatar className={classes.avatar}>
              <PeopleIcon fontSize="large" />
            </Avatar>
            <div className={classes.conversation_info}>
              <h1 className={classes.conversation_title}>&nbsp;&nbsp;Home</h1>
            </div>
          </div>

          <div className={classes.list_item}>
            <Avatar className={classes.avatar}>
              <CallReceivedIcon fontSize="large" />
            </Avatar>
            <div className={classes.conversation_info}>
              <h1 className={classes.conversation_title}>
                &nbsp;&nbsp;Friend requests
              </h1>
            </div>
          </div>

          <div className={classes.list_item}>
            <Avatar className={classes.avatar}>
              <PersonAddIcon fontSize="large" />
            </Avatar>
            <div className={classes.conversation_info}>
              <h1 className={classes.conversation_title}>
                &nbsp;&nbsp;Suggestions
              </h1>
            </div>
          </div>

          <div className={classes.list_item}>
            <Avatar className={classes.avatar}>
              <FormatAlignJustifyIcon fontSize="large" />
            </Avatar>
            <div className={classes.conversation_info}>
              <h1 className={classes.conversation_title}>
                &nbsp;&nbsp;All friends
              </h1>
            </div>
          </div>

          <div className={classes.list_item}>
            <Avatar className={classes.avatar}>
              <CakeIcon fontSize="large" />
            </Avatar>
            <div className={classes.conversation_info}>
              <h1 className={classes.conversation_title}>
                &nbsp;&nbsp;Birthdays
              </h1>
            </div>
          </div>
        </Paper>
  );
};

export default FriendsLeftBar;
