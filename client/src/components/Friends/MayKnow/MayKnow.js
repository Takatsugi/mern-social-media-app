import React, {useEffect} from "react";
import { Box, Grid, Typography, Button } from "@material-ui/core";
import useStyles from "./styles";
import avatar from "../../../images/avatar.png";
import { useDispatch } from 'react-redux';
import { getProfils } from '../../../actions/profil';
import { useSelector } from 'react-redux';

const MayKnow = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getProfils());
    if(profils === undefined){
      console.log('error')}else{ 
      // do stuff
  }
  }, []);
  const profils = useSelector(state => state.profil);

  console.log('profils ',profils)
  return (
    <div>
      <Typography
        style={{ margin: "20px" }}
        className={classes.typography}
        variant="h4"
      >
        People you may know
      </Typography>
      
      <Grid container spacing={0}>
            <Grid item >
      <Box className={classes.box} >
        <img className={classes.friendImage} src={avatar} alt="conversation" />
        <div style={{ margin: "10px" }}>
          <Typography className={classes.typography} variant="h6">
            Hatem Abrouz
          </Typography>
          <Grid container>
            <Grid item xs={1}>
              <img
                className={classes.mutualFriendImage}
                src={avatar}
                alt="conversation"
              />
            </Grid>
            <Grid>
              <p style={{ marginLeft: "10px", color: "gray" }}>
                1 mutual friend
              </p>
            </Grid>
          </Grid>
          <Button variant="contained" className={classes.buttonAdd}>
            Add friend
          </Button>
          <br />
          <Button variant="contained" className={classes.buttonRemove}>
            Remove
          </Button>
        </div>
      </Box>
      </Grid>
      <Box className={classes.box} >
        <img className={classes.friendImage} src={avatar} alt="conversation" />
        <div style={{ margin: "10px" }}>
          <Typography className={classes.typography} variant="h6">
            Hatem Abrouz
          </Typography>
          <Grid container>
            <Grid item xs={1}>
              <img
                className={classes.mutualFriendImage}
                src={avatar}
                alt="conversation"
              />
            </Grid>
            <Grid>
              <p style={{ marginLeft: "10px", color: "gray" }}>
                1 mutual friend
              </p>
            </Grid>
          </Grid>
          <Button variant="contained" className={classes.buttonAdd}>
            Add friend
          </Button>
          <br />
          <Button variant="contained" className={classes.buttonRemove}>
            Remove
          </Button>
        </div>
      </Box>
      </Grid>

      
      
      
    </div>
  );
};

export default MayKnow;
