import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  box: {
    width: 200,
        height: 380,
        backgroundColor: 'primary.dark',
        '&:hover': {
          backgroundColor: 'primary.main',
          opacity: '[0.9, 0.8, 0.7]',
        },
    margin: '10px',
    backgroundColor: 'white',
    borderRadius: '25px',
  },
  typography: {
    lineHeight: '0.6',
    fontWeight: '600',
    
  },
  friendImage: {
    width: 200,
        height: 200,
        borderTopLeftRadius: '25px',
        borderTopRightRadius: '25px'
  },
  mutualFriendImage: {
    width: '20px',
    height: '20px',
    borderRadius: '50%',
    objectFit: 'cover',
    marginRight: '10px',
    marginTop: '14px',
  },
  buttonAdd: {
    display: 'block',
    margin: '0 auto',
    backgroundColor: 'lightBlue',
    color: 'blue',
    minWidth: '100%',
    marginBottom: '-10px'
  },
  buttonRemove: {
    display: 'block',
    margin: '0 auto',
    minWidth: '100%',
  }
  
  
}));