import * as api from '../api';
import { FETCH_ALL,CREATE,DELETE,LIKEMESSAGE } from '../constants/actionTypes';

//action creators
export const getMessages = () => async (dispatch) => {
    try {
        // data = response from api
        const {data} = await api.fetchMessages();
        dispatch({ type: FETCH_ALL, payload: data})
    } catch (error) {
        console.log(error.message)
    }

    
}

export const createMessage = (message) => async (dispatch) => {
    try {
        const {data} = await api.createMessage(message);
        dispatch({ type: CREATE, payload: data})
    } catch (error) {
        console.log(error.message)
    }
}


  export const deleteMessage = (id) => async (dispatch) => {
    try {
      await api.deleteMessage(id);
  
      dispatch({ type: DELETE, payload: id });
    } catch (error) {
      console.log(error);
    }
  };

  export const likeMessage = (id) => async (dispatch) => {
    try {
      const { data } = await api.likeMessage(id);
  
      dispatch({ type: LIKEMESSAGE, payload: data });
    } catch (error) {
      console.log(error);
    }
  };