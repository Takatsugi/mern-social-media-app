import * as api from '../api';
import { FETCH_ALL, UPDATE } from '../constants/actionTypes';

//action creators
export const getProfils = () => async (dispatch) => {
    try {
        // data = response from api
        const {data} = await api.fetchProfils();
        dispatch({ type: FETCH_ALL, payload: data })
    } catch (error) {
        console.log(error.message)
    }

    
};

export const updateProfil = (profil) => async (dispatch) => {
    try {
      const { data } = await api.updateProfil(profil);
  
      dispatch({ type: UPDATE, payload: data });
    } catch (error) {
      console.log(error);
    }
  };